import { Component, OnInit } from '@angular/core';
import { TareaService } from "../../services/tarea.service";
@Component({
  selector: 'app-tarea-form',
  templateUrl: './tarea-form.component.html',
  styleUrls: ['./tarea-form.component.css']
})
export class TareaFormComponent implements OnInit {


  nombre: string;
  apellido: string;
  email: string;
  celular: string;
  fecha: string;
  hora: string;
  description: string;

  constructor(public taskService: TareaService) { }

  ngOnInit(): void {
  }


  Addtarea(newNombre: HTMLInputElement, newDescription: HTMLTextAreaElement, newApellido: HTMLInputElement,newEmail:HTMLInputElement, newCelular:HTMLInputElement,newFecha:HTMLInputElement,newHora:HTMLInputElement) {
    this.taskService.addTask({
      nombre: newNombre.value,
      apellido: newApellido.value,
      email: newEmail.value,
      celular: newCelular.value,
      fecha: newFecha.value,
      hora: newHora.value,
      description: newDescription.value,
      hide: true
    });



    newNombre.value = '';
    newDescription.value = '';
    newApellido.value = '';
    newEmail.value='';
    newCelular.value='';
    newFecha.value='';
    newHora.value='';
    return false;
  }

}
